// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyABB0b3VzPpakF4g8nnU6l-qHDPkw_MldI",
    authDomain: "angular-assignment-two.firebaseapp.com",
    databaseURL: "https://angular-assignment-two.firebaseio.com",
    projectId: "angular-assignment-two",
    storageBucket: "angular-assignment-two.appspot.com",
    messagingSenderId: "314289852131"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
