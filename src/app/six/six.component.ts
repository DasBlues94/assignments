import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-six',
  templateUrl: './six.component.html',
  styleUrls: ['./six.component.css']
})
export class SixComponent implements OnInit {
  public myform: FormGroup;
  constructor() { }

  ngOnInit() {
    this.myform = new FormGroup({
      email : new FormControl('', [
        Validators.required,
        Validators.pattern("^[a-z0-9._+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$")
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });
    console.log(this.myform.controls.password.errors);
  }

}
