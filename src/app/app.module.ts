import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/**
 * Components
 */

import { AppComponent } from './app.component';
import { OneComponent } from './one/one.component';
import { ThreeGoogleComponent } from './three-google/three-google.component';
import { ThreeFacebookComponent } from './three-facebook/three-facebook.component';
import { SixComponent } from './six/six.component';
import { NineComponent } from './nine/nine.component';
import { TenComponent } from './ten/ten.component';
import { SevenComponent } from './seven/seven.component';
import { ElevenComponent } from './eleven/eleven.component';

/**
 * Routing
 */

import { routing } from './app-routing.module';
import { RouterModule } from '@angular/router';

/**
 * Services
 */

import { HttpClientModule } from '@angular/common/http';
import { OneService } from './common/one.service';
import { TwoService } from './common/two.service';
import { FirebaseService } from './common/firebase.service';
/**
 * Modules for Firebase
 */
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

/**
 * importing environmental variable for firebase
 */
import { environment } from './../environments/environment';


/**
 * Module for importing social login
 */
import { SocialLoginModule, AuthServiceConfig } from 'angular5-social-login';
import { FacebookLoginProvider } from 'angular5-social-login';
import { getAuthServiceConfigs } from './common/socialLoginConfig';
import { FiveComponent } from './five/five.component';

/**
 * agm core for google maps
 */
import { AgmCoreModule } from '@agm/core';

/**
 * Modules for Cloudinary
 */
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as  Cloudinary from 'cloudinary-core';

/**
 * Module for file upload
 */
import { FileUploadModule } from 'ng2-file-upload';
import { EightComponent } from './eight/eight.component';
import { CountryService } from './common/country.service';
import { FourComponent } from './four/four.component';


@NgModule({
  declarations: [
    AppComponent,
    OneComponent,
    ThreeGoogleComponent,
    ThreeFacebookComponent,
    FiveComponent,
    SixComponent,
    NineComponent,
    TenComponent,
    SevenComponent,
    ElevenComponent,
    EightComponent,
    FourComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    routing,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    SocialLoginModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyACbPN3suYUJknSBLg8IzpWf4XbloGqtAc',
      libraries: ['places']
    }),
    CloudinaryModule.forRoot(Cloudinary, { cloud_name: 'soumyanil', upload_preset:'idbhf8rq'})
  ],
  providers: [
    RouterModule,
    OneService,
    TwoService,
    CountryService,
    FirebaseService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
