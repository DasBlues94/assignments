import { Routes, RouterModule } from '@angular/router';
import { OneComponent } from './one/one.component';
import { ThreeGoogleComponent } from './three-google/three-google.component';
import { ThreeFacebookComponent } from './three-facebook/three-facebook.component';
import { FiveComponent } from './five/five.component';
import { SixComponent } from './six/six.component';
import { NineComponent } from './nine/nine.component';
import { TenComponent } from './ten/ten.component';
import { SevenComponent } from './seven/seven.component';
import { ElevenComponent } from './eleven/eleven.component';
import { EightComponent } from './eight/eight.component';
import { FourComponent } from './four/four.component';


const appRoutes: Routes = [
    { 
        path : 'one', 
        component: OneComponent
    },
    {
        path: 'two',
        loadChildren: './two/two.module#TwoModule'
    },
    {
        path: 'three-google',
        component: ThreeGoogleComponent
    },
    {
        path: 'three-facebook',
        component: ThreeFacebookComponent
    },
    {
        path: 'four',
        component: FourComponent
    },
    {
        path: 'five',
        component: FiveComponent
    },
    {
        path: 'six',
        component: SixComponent
    },
    {
        path: 'seven',
        component: SevenComponent
    },
    {
        path: 'nine',
        component: NineComponent

    },
    {
        path: 'eight',
        component: EightComponent
    },
    {
        path: 'ten',
        component: TenComponent
    },
    {
        path: 'eleven',
        component: ElevenComponent
    },
    {
        path: '', 
        redirectTo:'/one', 
        pathMatch:'full' 
    }
];

export const routing = RouterModule.forRoot(appRoutes);