import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eleven',
  templateUrl: './eleven.component.html',
  styleUrls: ['./eleven.component.css']
})
export class ElevenComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var OneSignal = window['OneSignal'] || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: "2c1ff2a9-0ab7-4f49-acf2-2e570b6a5fe1",
        autoRegister: false,
        notifyButton: {
          enable: true,
        },
      });
    });
  }

}
