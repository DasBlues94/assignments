import { Component, OnInit } from '@angular/core';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular5-social-login';
import { FirebaseService } from '../common/firebase.service';
@Component({
  selector: 'app-three-facebook',
  templateUrl: './three-facebook.component.html',
  styleUrls: ['./three-facebook.component.css']
})
export class ThreeFacebookComponent implements OnInit {

  constructor(private socialAuthService: AuthService, private fireBaseService: FirebaseService) { }

  public facebookLogin() {
    let socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData:any) => {
              // console.log(userData);              
              console.log("Name:", userData.name);
              console.log("Email:", userData.email);
              console.log("Image:", userData.image);
       }
    );
    this.fireBaseService.doFacebookLogin();    
  }
  

  ngOnInit() {
  }

}
