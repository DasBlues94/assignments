import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeFacebookComponent } from './three-facebook.component';

describe('ThreeFacebookComponent', () => {
  let component: ThreeFacebookComponent;
  let fixture: ComponentFixture<ThreeFacebookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreeFacebookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeFacebookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
