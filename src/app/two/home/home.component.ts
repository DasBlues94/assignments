import { Component, OnInit } from '@angular/core';
import { TwoService } from '../../common/two.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public allCustomers:any;
  public customer:any = {};
  public test:boolean = true;
  constructor(private two: TwoService) {
    this.two.getCustomers()
    .subscribe(
      (response:any) => {
        this.allCustomers = response;
      } 
    );
   }
  
   deleteCustomer(id:string){
    this.two.deleteCustomer(id);  
  }

  submitForm() {
    this.two.insertCustomers(this.customer); 
    this.customer = {
      company:"",
      contact:"",
      country:""
    };
  }
  ngOnInit() {
  }

}
