import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
const appRoutes: Routes = [
    { 
        path: '', 
        component: HomeComponent
    },
    {
        path: 'edit/:id',
        component: EditComponent
    },
    { 
        path: '', 
        redirectTo:'/two', 
        pathMatch:'full' 
    }
];

export const routing = RouterModule.forChild(appRoutes);