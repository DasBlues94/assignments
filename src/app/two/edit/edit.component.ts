import { Component, OnInit } from '@angular/core';
import { TwoService } from '../../common/two.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public id:string;
  public customer:any = {};
  constructor(private two:TwoService, private route:ActivatedRoute) {
    route.params.subscribe(
      (params:any) => {
        this.id = params.id;
      }
    );
    this.two.getCustomers()
    .subscribe(
      (response:any) => {
        const result = (response.filter(l => l.key === this.id ));        
        this.customer = result[0].payload.val();
        
      }
    );
   }
   submitForm(){
     this.two.updateCustomer(this.id,this.customer);
   }
  ngOnInit() {
  }

}
