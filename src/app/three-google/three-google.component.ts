import { Component, OnInit, AfterViewInit } from '@angular/core';
declare const gapi: any;

@Component({
  selector: 'app-three-google',
  templateUrl: './three-google.component.html',
  styleUrls: ['./three-google.component.css']
})
export class ThreeGoogleComponent implements OnInit,AfterViewInit {

  constructor() { }

  ngOnInit() {
  }
  private scope = [
    'profile',
    'email',
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/contacts.readonly',
    'https://www.googleapis.com/auth/admin.directory.user.readonly'
  ].join(' ');

  public auth2: any;
  public googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '414231659858-bsnd4tmqh78k90stlcjul49vv1ti7e5d.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: this.scope
      });
      this.attachSignin(document.getElementById('googleBtn'));
    });
  }
  public attachSignin(element) {
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {

        // this.profile = googleUser.getBasicProfile();
        
        let profile = googleUser.getBasicProfile();
        console.log('Token || ' + googleUser.getAuthResponse().id_token);
        console.log('ID: ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());
        //YOUR CODE HERE


      }, (error) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }

ngAfterViewInit(){
      this.googleInit();
}

}
