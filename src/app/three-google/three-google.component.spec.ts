import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeGoogleComponent } from './three-google.component';

describe('ThreeGoogleComponent', () => {
  let component: ThreeGoogleComponent;
  let fixture: ComponentFixture<ThreeGoogleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreeGoogleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeGoogleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
