import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class OneService {

  constructor(private http: HttpClient) { }

  getCustomers():Observable<any>{
    return this.http.get("./assets/one.json")
  }
}
