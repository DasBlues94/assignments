import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'; 
import { Observable } from 'rxjs';

@Injectable()
export class TwoService {

  constructor(private db: AngularFireDatabase) { }

  getCustomers():Observable<any>{
    return this.db.list('/customers').snapshotChanges();
  }
  insertCustomers(customer:any){
    return this.db.list('/customers').push(customer)
  }
  updateCustomer(id:string,data:any){
    return this.db.list('/customers').update(id,data);
  }
  deleteCustomer(id:string){
    return this.db.list('/customers').remove(id);
  }

}
