import { Component, OnInit } from '@angular/core';
import { OneService } from '../common/one.service';


@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit {
  public allCustomers:any;
  constructor(private one: OneService) { 
    this.one.getCustomers()
    .subscribe(
      (response:any) => { 
        this.allCustomers = response.customers;
        console.log(response.customers);
      }
    );
  }

  ngOnInit() {
  }


}
