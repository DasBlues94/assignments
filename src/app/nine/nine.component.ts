import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-nine',
  templateUrl: './nine.component.html',
  styleUrls: ['./nine.component.css']
})
export class NineComponent implements OnInit {

  public YT: any;
  public video: any;
  public player: any;
  public reframed: Boolean = false;
  public safeURL : any;
  constructor(private sanitizer: DomSanitizer) { 
    // this.safeURL = sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/lt-udg9zQSE?modestbranding=1&showinfo=0')
  }

  init() {
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  ngOnInit() {
    this.init();
    this.video = 'lt-udg9zQSE' //video id

    window['onYouTubeIframeAPIReady'] = (e) => {
      this.YT = window['YT'];
      this.reframed = false;
      this.player = new window['YT'].Player('player', {
        videoId: this.video,
        playerVars: {
          showinfo: 0, 
          modestbranding: 1,
          start: localStorage.getItem('pause-time')
        },
        events: {
          'onStateChange': this.onPlayerStateChange.bind(this),
          'onReady': (e) => {
            if (!this.reframed) {
              this.reframed = true;
              //reframe(e.target.a);
            }
          }
        }
      });
    }
  }
  onPlayerStateChange(event) {
    console.log("Hello..", event)
    switch (event.data) {
      case window['YT'].PlayerState.PLAYING:
        if (this.cleanTime() == 0) {
          console.log('started ' + this.cleanTime());
        } else {
          console.log('playing ' + this.cleanTime())
        }
        break;
      case window['YT'].PlayerState.PAUSED:
        if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
          console.log('paused' + ' @ ' + this.cleanTime());
          localStorage.setItem('pause-time', this.cleanTime().toString());
        }
        break;
      case window['YT'].PlayerState.ENDED:
        console.log('ended ');
        break;
    }
  }
  //utility
  cleanTime() {
    return Math.round(this.player.getCurrentTime());
  }

}
