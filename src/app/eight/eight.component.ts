import { Component, OnInit } from '@angular/core';
import { CountryService } from '../common/country.service';

@Component({
  selector: 'app-eight',
  templateUrl: './eight.component.html',
  styleUrls: ['./eight.component.css']
})
export class EightComponent implements OnInit {

  public countrylist;
  public flag = true;
  public countryName;
  public filteredList;
  public countryCode;
  constructor(private countryService: CountryService) { 
    this.countryService.getJSON()
    .subscribe( response => {
      this.countrylist = response;
    });    
  }
  showCountry(){
    //console.log(this.countryName);
    if(this.countryName.trim() === '')
      this.flag = true;
    else
      this.flag = false;
    this.filteredList = this.countrylist.filter(country => country.name.includes(this.countryName)||country.callingCodes.includes(this.countryName));
  }
  getCountry(html:string) {
    // console.log();
    this.countryCode = html.split(" ")[5];
    this.flag = true;
  }

  ngOnInit() {
  }

}
