import { Component, OnInit } from '@angular/core';
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ten',
  templateUrl: './ten.component.html',
  styleUrls: ['./ten.component.css']
})
export class TenComponent implements OnInit {

  public audio;
  public subscription: Subscription;
  public tick:string = "00:00";
  public timer;
  public minutes;
  public seconds;

  constructor() { }

  ngOnInit() {
    this.audio = new Audio();
    this.audio.src = "assets/1.mp3";
    this.audio.load();
    this.timer = TimerObservable.create(1000,1000);
  }
  play() {
    this.audio.play();
    this.subscription = this.timer.subscribe( () => {
        console.log(this.audio.currentTime);        
        let m = Math.floor(parseFloat(this.audio.currentTime) / 60);
        this.minutes = (m >= 10) ? m : "0" + m;
        let s = Math.floor(parseFloat(this.audio.currentTime) % 60);
        this.seconds = (s >= 10) ? s : "0" + s;
        this.tick = this.minutes + ":" + this.seconds;
    });
  }
  pause() {
    this.audio.pause();
    this.audio.currentTime = this.audio.currentTime - 0.5;
    this.subscription.unsubscribe();
  }
  stop() {
    this.audio.pause();
    this.audio.currentTime = 0;
    this.subscription.unsubscribe();
    this.tick = "00:00";
  }

}
