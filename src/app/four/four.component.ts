import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
declare let Razorpay: any;

@Component({
  selector: 'app-four',
  templateUrl: './four.component.html',
  styleUrls: ['./four.component.css']
})
export class FourComponent implements OnInit {

  public customer:any = {};
  public quantity = 1;
  constructor() { }

  ngOnInit() {
  }

  initPay() {
    let options = {
      "key": "rzp_test_qkutRdxacotole",
      "amount": 10000 * this.quantity, 
      "name": "Soumyanil Das",
      "description": "Purchase Description",
      "handler": function (response){
          console.log(response.razorpay_payment_id);
      },
      "prefill": {
          "name": this.customer.name,
          "email": this.customer.email
      },
      "notes": {
          "address": "Hello World"
      },
      "theme": {
          "color": "#F37254"
      }
    };
    let rzp1 = new Razorpay(options);
    rzp1.open();
  }

}
