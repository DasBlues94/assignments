import { Component, OnInit } from '@angular/core';
import { Cloudinary } from '@cloudinary/angular-5.x';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';

@Component({
  selector: 'app-seven',
  templateUrl: './seven.component.html',
  styleUrls: ['./seven.component.css']
})
export class SevenComponent implements OnInit {
  public src;
  public innerHeight;
  public innerWidth;
  constructor(private cloudinary: Cloudinary) { 
    // console.log(this.cloudinary.config().cloud_name);
      this.innerHeight = (window.screen.height);
      this.innerWidth = (window.screen.width);
      console.log(this.innerHeight);
  }

  imageId: string;

  uploader: CloudinaryUploader = new CloudinaryUploader(
      new CloudinaryOptions({ cloudName: 'soumyanil', uploadPreset: 'idbhf8rq' })
  );

  // uploader = new FileUploader({
  //   url: `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/upload`,
  //   isHTML5: true,
  // });
  ngOnInit() {
      this.uploader.onAfterAddingFile = (file) => {
        console.log('***** onAfterAddingFile ******')
        console.log('file ', file)
      }

      this.uploader.onCompleteItem =  (item:any, response:any, status:any, headers:any) => {
        console.log('ImageUpload:uploaded:', item, status, response);
        const r = JSON.parse(response);
        this.src = 'https://res.cloudinary.com/soumyanil/image/upload/' + 'c_scale,w_' + this.innerWidth + '/' + (r.url.split('http://res.cloudinary.com/soumyanil/image/upload/')[1]);
      };

      this.uploader.onCompleteAll = () => {
        console.log('******* onCompleteAll *********');
      }

      this.uploader.onWhenAddingFileFailed = (item: any, filter: any, options: any) => {
        console.log('***** onWhenAddingFileFailed ********')
      }
  }
}
